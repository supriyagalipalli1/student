package com.dto;

public class Student {
	private int id;
	private String name;
	private String gender;
	private String email;
	private String password;
	
	public Student() {
		super();
	}

	public Student(int id, String name, String gender, String email, String password) {
		super();
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.email = email;
		this.password = password;
	}

	public int getid() {
		return id;
	}
	public void setid(int id) {
		this.id = id;
	}

	public String getname() {
		return name;
	}
	public void setname(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ",  gender=" + gender
				+ ", email=" + email + ", password=" + password + "]";
	}
}

